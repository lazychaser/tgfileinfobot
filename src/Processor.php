<?php

class Processor
{
    public function __construct(protected readonly \Telegram\Bot\Api $api)
    {
        //
    }

    public function processMessage(\Telegram\Bot\Objects\Message $msg): void {
        if ($msg->text == 'ping') {
            $this->reply($msg, [ 'text' => 'pong' ]);
        }

        foreach (['video', 'voice', 'animation', 'audio', 'sticker', 'video_note'] as $attr) {
            if ($value = $msg->get($attr)) {
                $this->reply($msg, [
                    'text' => <<<EOL
chatId = {$msg->chat->id}
fileId = {$value->fileId}
fileUniqueId = {$value->fileUniqueId}
fileName = {$value->fileName}
fileSize = {$value->fileSize}
EOL,
                ]);
            }
        }
    }

    protected function reply(\Telegram\Bot\Objects\Message $msg, array $payload): void
    {
        $this->api->sendMessage([
            'chat_id' => $msg->chat->id,
            ...$payload,
        ]);
    }
}