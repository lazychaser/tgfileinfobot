<?php

require __DIR__.'/../vendor/autoload.php';
require __DIR__.'/Processor.php';

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Telegram\Bot\Api;

$app = \Slim\Factory\AppFactory::create();

$processor = new Processor($bot = new Api(trim(file_get_contents(__DIR__.'/../token'))));

$app->get('/', function (Request $request, Response $response) use ($bot) {
    $response->getBody()->write("Getting bot info...<br>");

    try {
        $info = $bot->getMe();

        $response->getBody()->write("Bot name: {$info->username}<br>");
    }

    catch (\Telegram\Bot\Exceptions\TelegramSDKException $e) {
        $response->getBody()->write("Error: {$e->getMessage()}");
    }

    return $response;
});

$app->post('/callback', function (Request $request, Response $response) use ($bot, $processor) {
    $update = $bot->getWebhookUpdate(shouldEmitEvent: false, request: $request);

    match ($update->objectType()) {
        'message' => $processor->processMessage($update->message),
    };

    return $response;
});

$app->get('/setup', function (Request $request, Response $response) use ($bot) {
    $url = 'https://'.$request->getHeaderLine('host').'/callback';

    $bot->setWebhook([
        'url' => $url,
    ]);

    $response->getBody()->write("Webhook url is set to $url");

    return $response;
});

$app->run();